/** @format */

import {
  Drawer,
  Icon,
  Badge,
  Table,
  Tabs,
  List,
  message,
  Avatar,
  Spin,
  notification,
} from "antd";
import React from "react";
import { Message } from "./message";
import { notificationTableFun } from "./columns";
import InfiniteScroll from "react-infinite-scroll-component";
import reqwest from "reqwest";
const fakeDataUrl =
  "https://randomuser.me/api/?results=5&inc=name,gender,email,nat&noinfo";
const { TabPane } = Tabs;
let _ = require("lodash");

class Sidercontent extends React.Component {
  state = {
    visible: false,
    data: [],
    items: [],
    loading: false,
    hasMore: true,
  };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };
  // infinite scroller function

  //infinite scroller function ends over here

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  componentDidMount() {
    this.fetchData((res) => {
      this.setState({
        items: res.results,
      });
    });
  }
  fetchData = (callback) => {
    reqwest({
      url: fakeDataUrl,
      type: "json",
      method: "get",
      contentType: "application/json",
      success: (res) => {
        callback(res);
      },
    });
  };
  handleInfiniteOnLoad = () => {
    console.log("this is fired");
    let { items } = this.state;
    this.setState({
      loading: true,
    });
    if (items.length > 14) {
      message.warning("Infinite List loaded all");
      this.setState({
        hasMore: false,
        loading: false,
      });
      return;
    }
    this.fetchData((res) => {
      items = items.concat(res.results);
      this.setState({
        items,
        loading: false,
      });
    });
  };
  componentWillReceiveProps = (props) => {
    var grouped = _.mapValues(
      _.groupBy(props.logs, "eventTimestamp"),
      (clist) => clist.map((prop) => _.omit(prop, "eventTimestamp"))
    );
    this.setState({ data: grouped });
    console.log(grouped);
  };

  render() {
    const tabone = `Current Submission`;
    const tabtwo = `All (${this.props.logs.length})`;
    const notifications = [];

    for (const key in this.state.data) {
      const data = [];
      this.state.data[key].map((log) => {
        const badge = log.isRead ? null : <Badge color="cyan" />;

        data.push({
          elementname: log.elementName,
          keyname: log.fieldName,
          oldvalue: log.oldValue,
          newvalue: log.newValue,
        });
        notifications.push(
          <div>
            <div style={{ display: "flex" }}>
              {Message(log, key)}
              <span>{badge}</span>
            </div>
            <hr style={{ borderTop: "1px dotted black" }} />
          </div>
        );
      });

      if (this.state.data[key].length >= 3) {
        notifications.push(
          <div className="nestedtable">
            <Table
              columns={notificationTableFun()}
              dataSource={data}
              size="small"
            />
          </div>
        );
      }
    }

    return (
      <div>
        <Icon
          className="bellicon"
          type="bell"
          theme="twoTone"
          onClick={this.showDrawer}
        />

        <Drawer
          title={
            <div style={{ display: "flex" }}>
              <span
                style={{
                  fontSize: "1vw",
                  justifySelf: "flex-start",
                  alignSelf: "flex-end",
                }}
              >
                Notifications({this.props.logs.length})
              </span>
              <a
                type="none"
                href="null"
                style={{
                  border: "none",
                  fontSize: "1vw",
                  marginTop: "1vw",
                  marginLeft: "19vw",
                }}
              >
                Show All
              </a>
            </div>
          }
          width="550"
          placement="left"
          closable={true}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <Tabs defaultActiveKey="1">
            <TabPane tab={tabone} key="1">
              Content of Tab Pane 2
            </TabPane>
            <TabPane tab={tabtwo} key="2">
              {notifications}
              {/*this is the code that will reverse infinity scroll*/}

              {/* {Infinity starts from herer} */}

              {/* Infinity ends here */}
            </TabPane>
          </Tabs>
        </Drawer>
      </div>
    );
  }
}

export default Sidercontent;
