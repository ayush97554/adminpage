/** @format */

import React from "react";

export const Message = (log, eventTimestamp) => {
  if (log) {
    let message = log.notificationMessage;
    let startBoldTagRegexp = /<bold>/i;
    let endBoldTagRegexp = /<\/+bold>/i;
    let startBoldTag = message.match(startBoldTagRegexp);
    if (startBoldTag) {
      let endBoldTag = message.match(endBoldTagRegexp);
      let startOfBoldIndex = startBoldTag.index + startBoldTag[0].length;
      let endOfBoldIndex = endBoldTag.index;
      let boldText = message.slice(startOfBoldIndex, endOfBoldIndex);
      let textBeforeBold = message.slice(0, startBoldTag.index);
      let textAfterBold = message.slice(
        endOfBoldIndex + endBoldTag[0].length,
        message.length
      );
      let submissionIdRegexp = /[0-9]/g;
      let submissionId = parseInt(
        message.match(submissionIdRegexp).join(" ").replace(/ /g, "")
      );
      console.log(typeof submissionId);
      return (
        <div style={{ fontSize: "1vw", fontWeight: "bold", width: "100%" }}>
          {textBeforeBold}&nbsp;
          <a onClick={() => alert("hello")}>{boldText}</a>
          &nbsp;{textAfterBold}&nbsp;
          <a>{eventTimestamp}</a>
        </div>
      );
    } else {
      return (
        <div style={{ fontSize: "1vw", fontWeight: "bold", width: "100%" }}>
          {message}&nbsp;
          <a>{eventTimestamp}</a>
        </div>
      );
    }
  } else return null;
};
