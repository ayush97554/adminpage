/** @format */

import React from "react";
export const notificationTableFun = () => {
  const columns = [
    {
      title: "Element Name",
      dataIndex: "elementname",
      defaultSortOrder: "descend",
    },
    {
      title: "Key Name ",
      dataIndex: "keyname",
    },
    {
      title: "Old Value",
      dataIndex: "oldvalue",
    },
    {
      title: "New Value",
      dataIndex: "newvalue",
    },
  ];
  return columns;
};
