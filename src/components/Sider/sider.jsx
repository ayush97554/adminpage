import React, { Component } from 'react';
import Sidercontent from '../Sidercontent/sidercontent';
class Sider extends Component {
  render() { 
    return(
      <div className="sider">
        <Sidercontent logs={this.props.logs} displayNestedTable={this.props.displayNestedTable}/>
    </div> 
    );
  }
}
 
export default Sider;