/** @format */

import React, { Component } from "react";

import { Alert } from "antd";
class Errorhandler extends Component {
  state = {};
  render() {
    return (
      <div>
        <Alert
          type="error"
          message={"Failed to fetch Data Check  your internet connection"}
          banner
        />
      </div>
    );
  }
}

export default Errorhandler;
