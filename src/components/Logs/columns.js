/** @format */

import React from "react";
export const notificationColumnsFun = () => {
  const columns = [
    {
      title: "Submission Id",
      dataIndex: "eventid",
      width: 150,
      defaultSortOrder: "descend",
      render: (text) => <a>{text}</a>,
      sorter: (a, b) => a.eventid - b.eventid,
    },
    {
      title: "Event Time Stamp ",
      dataIndex: "date",
      width: 200,
      defaultSortOrder: "descend",
      sorter: (a, b) => a.date[0] - b.date[0],
    },
    {
      title: "User",
      dataIndex: "user",
      width: 150,
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Element Name",
      dataIndex: "elementName",
      width: 150,
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Old Value",
      dataIndex: "oldvalue",
      width: 140,
      sorter: (a, b) => a.oldvalue - b.oldvalue,
    },
    {
      title: "New Value",
      dataIndex: "newvalue",
      width: 140,
      sorter: (a, b) => a.newvalue - b.newvalue,
    },
    {
      title: "Message",
      dataIndex: "Message",
    },
  ];
  return columns;
};
