/** @format */

import { Component } from "react";
import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import { notificationColumnsFun } from "./columns";

class NestedTable extends Component {
  state = {
    data: [],
  };

  componentWillReceiveProps = (props) => {
    const data = [];
    for (let i = 0; i < props.logs.length; i++) {
      data.push({
        key: i,
        eventid: props.logs[i].eventId,
        date: props.logs[i].date,
        oldvalue: props.logs[i].oldValue,
        newvalue: props.logs[i].newValue,
        user: props.logs[i].user,
        Message: props.logs[i].Message,
      });
    }
    this.setState({ data: data });
  };
  render() {
    return (
      <div className="nestedtable">
        <Table
          columns={notificationColumnsFun()}
          dataSource={this.state.data}
          size="small"
        />
      </div>
    );
  }
}

export default NestedTable;
