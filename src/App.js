/** @format */
import React from "react";
import { Component } from "react";
import "./App.css";
import NestedTable from "./components/Logs/logs";
import { Input } from "antd";
import SiderDemo from "./components/Sider/sider";
import Errorhandler from "./components/Errorhandler/Errorhandler";
import { Button } from "antd";

import { Tabs } from "antd";

const { TabPane } = Tabs;
const { Search } = Input;

class App extends Component {
  state = {
    logs: [],
    notifications: [],
    searchedLogs: [],
    found: true,
    renderNestedTable: null,
    error: {},
    isError: false,
  };
  componentDidMount = () => {
    fetch("https://my-json-server.typicode.com/ayush17/database/logs")
      .then((res) => res.json())
      .then((log) => this.setState({ logs: log }))
      .catch((err) => {
        this.setState({ error: err, isError: true });
      });

    fetch("https://my-json-server.typicode.com/ayush17/database/notifications")
      .then((res) => {
        return res.json();
      })
      .then((notification) => {
        return this.setState({ notifications: notification });
      })
      .catch((err) => {
        this.setState({ error: err, isError: true });
      });
  };
  searchEvent = (value) => {
    if (value) {
      const searchedLogs = this.state.logs.filter(
        (element) => element.eventId == value
      );
      if (searchedLogs.length) {
        this.setState({ searchedLogs: searchedLogs, found: true });
      } else {
        searchedLogs.push(["not found"]);
        this.setState({ searchedLogs: searchedLogs, found: false });
      }
    } else {
      this.setState({ searchedLogs: [], found: false });
    }
  };
  displayNestedTable = (event) => {
    event.preventDefault();
    fetch("https://my-json-server.typicode.com/ayush17/database/logs")
      .then((res) => res.json())
      .then((log) => this.setState({ logs: log }))
      .catch((err) => {
        this.setState({ error: err, isError: true });
        console.log("Failed to fetch data->", err);
      });
  };
  render() {
    const tabone = `All (${this.state.logs.length})`;
    const tabtwo = `Pinned Submission`;
    return (
      <React.Fragment>
        {this.state.isError ? (
          <Errorhandler />
        ) : (
          <div className="App">
            <SiderDemo
              logs={this.state.notifications}
              displayNestedTable={this.displayNestedTable}
            />
            <div className="notifications">
              <div className="notificationtext">
                <span>Notifications ({this.state.logs.length})</span>
              </div>
              <br />
              <Tabs defaultActiveKey="1">
                <TabPane tab={tabone} key="1">
                  <Search
                    placeholder="Search By Submission"
                    onSearch={(value) => this.searchEvent(value)}
                    className="searchbar"
                  />
                  <NestedTable
                    logs={
                      this.state.searchedLogs.length
                        ? this.state.found
                          ? this.state.searchedLogs
                          : []
                        : this.state.logs
                    }
                  />
                </TabPane>
                <TabPane tab={tabtwo} key="2">
                  Content of Tab Pane 2
                </TabPane>
              </Tabs>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default App;
